
# Main Heading

---

## Sub Heading

### Sub Heading 2

#### Sub Heading 3

##### Sub Heading 4

###### Sub Heading 5

---

**This is bold text**

*This is italic text*

~~This is strikethrough text~~

---

> This is a blockquote

---

`This is inline code`

```javascript
// This is a JS code block
const foo = () => {
  return 'bar';
}
```

```bash
# This is a bash code block
echo 'Hello World'

cat <<-EOF
This is a heredoc
EOF
```

```python
# This is a python code block
def foo():
  return 'bar'
```

---

- This is a list item
- This is list item 2
  - This is a nested list item
  - This is a nested list item 2
    - This is a nested list item 3

---

1. This is an ordered list item
2. This is an ordered list item 2
   1. This is a nested ordered list item
   2. This is a nested ordered list item 2
      1. This is a nested ordered list item 3


| This is a table | This is a table | This is a table |
| --------------- | --------------- | --------------- |
| This is a table | This is a table | This is a table |
| This is a table | This is a table | This is a table |
| This is a table | This is a table | This is a table |

---

[This is a link to google](https://google.com)

---

![This is an image](https://picsum.photos/800/800)

---

This is a footnote[^1]

[^1]: This is a footnote


