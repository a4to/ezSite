const fs = require('fs');
const marked = require('marked');

const markdownToHtml = (markdownFile, outputFile, cssFile) => {
    fs.readFile(markdownFile, 'utf8', (err, markdown) => {
        if (err) {
            console.error(err);
            return;
        }
        fs.readFile(cssFile, 'utf8', (err, css) => {
            if (err) {
                console.error(err);
                return;
            }
            const html = `<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Markdown Converted</title>
    <style>${css}</style>
  </head><body>
    ${marked.parse(markdown)}
  </body>
</html>
`;
            fs.writeFile(outputFile, html, err => {
                if (err) {
                    console.error(err);
                }
            });
        });
    });
};

const inputMarkdownFile = process.argv[2] || 'index.md';
const outputHtmlFile = process.argv[3] || 'index.html';
const cssFile = process.argv[4] || 'styles.css';

markdownToHtml(inputMarkdownFile, outputHtmlFile, cssFile);

